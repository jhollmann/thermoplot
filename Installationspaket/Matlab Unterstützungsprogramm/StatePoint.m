classdef StatePoint
   properties
      T
      P
      D
      H
      S
      Q
      Subst
      Status = 'Init'
   end
   methods
%% T Kombis
       function obj = DefineBy_TP(obj)
          [obj.H,obj.S,obj.Q,obj.D] = refpropm('HSQD','T',obj.T,'P',obj.P,obj.Subst);
          obj.Status = 'Defined by User Input TP';
       end

       function obj = DefineBy_TD(obj)
          [obj.H,obj.S,obj.Q,obj.P] = refpropm('HSQP','T',obj.T,'D',obj.D,obj.Subst);
          obj.Status = 'Defined by User Input TD';
       end
       
       function obj = DefineBy_TH(obj)
          [obj.P,obj.S,obj.Q,obj.D] = refpropm('PSQD','T',obj.T,'H',obj.H,obj.Subst);
          obj.Status = 'Defined by User Input TH';
       end
       
       function obj = DefineBy_TS(obj)
          [obj.H,obj.P,obj.Q,obj.D] = refpropm('HPQD','T',obj.T,'S',obj.S,obj.Subst);
          obj.Status = 'Defined by User Input TS';
       end
       
       function obj = DefineBy_TQ(obj)
          [obj.H,obj.S,obj.P,obj.D] = refpropm('HSPD','T',obj.T,'Q',obj.Q,obj.Subst);
          obj.Status = 'Defined by User Input TQ';
       end
 %% P Kombis
      
%      function obj = DefineBy_PT(obj)
%         [obj.Q,obj.D,obj.H,obj.S] = refpropm('QDHS','P',obj.P,'T',obj.T,obj.Subst);
%         obj.Status = 'Defined by User Input PT';
%      end

      function obj = DefineBy_PQ(obj)
         [obj.T,obj.D,obj.H,obj.S] = refpropm('TDHS','P',obj.P,'Q',obj.Q,obj.Subst);
         obj.Status = 'Defined by User Input PQ';
      end
      
      function obj = DefineBy_PD(obj)
         [obj.T,obj.Q,obj.H,obj.S] = refpropm('TQHS','P',obj.P,'D',obj.D,obj.Subst);
         obj.Status = 'Defined by User Input PD';
      end
      
      function obj = DefineBy_PH(obj)
         [obj.T,obj.D,obj.Q,obj.S] = refpropm('TDQS','P',obj.P,'H',obj.H,obj.Subst);
         obj.Status = 'Defined by User Input PH';
      end
      
      function obj = DefineBy_PS(obj)
         [obj.T,obj.D,obj.H,obj.Q] = refpropm('TDHQ','P',obj.P,'S',obj.S,obj.Subst);
         obj.Status = 'Defined by User Input PS';
      end
      
 %% Q Kombis     
 
%       function obj = DefineBy_QT(obj)
%          [obj.D,obj.P,obj.H,obj.S] = refpropm('DPHS','Q',obj.Q,'T',obj.T,obj.Subst);
%          obj.Status = 'Defined by User Input QT';
%       end
      
      function obj = DefineBy_QD(obj)
         [obj.T,obj.P,obj.H,obj.S] = refpropm('TPHS','Q',obj.Q,'D',obj.D,obj.Subst);
         obj.Status = 'Defined by User Input QD';
      end
      
%       function obj = DefineBy_QP(obj)
%          [obj.T,obj.D,obj.H,obj.S] = refpropm('TDHS','Q',obj.Q,'P',obj.P,obj.Subst);
%          obj.Status = 'Defined by User Input QP';
%       end
      
      function obj = DefineBy_QH(obj)
         [obj.T,obj.P,obj.D,obj.S] = refpropm('TPDS','Q',obj.Q,'H',obj.H,obj.Subst);
         obj.Status = 'Defined by User Input QH';
      end
      
      function obj = DefineBy_QS(obj)
         [obj.T,obj.P,obj.H,obj.D] = refpropm('TPHD','Q',obj.Q,'S',obj.S,obj.Subst);
         obj.Status = 'Defined by User Input QS';
      end 
%% D Kombis      
%       function obj = DefineBy_DQ(obj)
%          [obj.T,obj.P,obj.H,obj.S] = refpropm('TPHS','D',obj.D,'Q',obj.Q,obj.Subst);
%          obj.Status = 'Defined by User Input DQ';
%       end
      
%       function obj = DefineBy_DT(obj)
%          [obj.Q,obj.D,obj.H,obj.S] = refpropm('QDHS','D',obj.D,'T',obj.T,obj.Subst);
%          obj.Status = 'Defined by User Input DT';
%       end
      
%       function obj = DefineBy_DP(obj)
%          [obj.T,obj.Q,obj.H,obj.S] = refpropm('TQHS','D',obj.D,'P',obj.P,obj.Subst);
%          obj.Status = 'Defined by User Input DP';
%       end
      
      function obj = DefineBy_DH(obj)
         [obj.T,obj.P,obj.Q,obj.S] = refpropm('TPQS','D',obj.D,'H',obj.H,obj.Subst);
         obj.Status = 'Defined by User Input DH';
      end    

      function obj = DefineBy_DS(obj)
         [obj.T,obj.P,obj.H,obj.Q] = refpropm('TPHQ','D',obj.D,'S',obj.S,obj.Subst);
         obj.Status = 'Defined by User Input DS';
      end  
%% H Kombis
%        function obj = DefineBy_HT(obj)
%           [obj.D,obj.P,obj.Q,obj.S] = refpropm('DPQS','H',obj.H,'T',obj.T,obj.Subst);
%           obj.Status = 'Defined by User Input HT';
%        end    

%        function obj = DefineBy_HP(obj)
%           [obj.T,obj.D,obj.Q,obj.S] = refpropm('TDQS','H',obj.H,'P',obj.P,obj.Subst);
%           obj.Status = 'Defined by User Input HP';
%        end
      
%        function obj = DefineBy_HQ(obj)
%           [obj.T,obj.P,obj.D,obj.S] = refpropm('TPDS','H',obj.H,'Q',obj.Q,obj.Subst);
%           obj.Status = 'Defined by User Input HQ';
%        end    

%        function obj = DefineBy_HD(obj)
%           [obj.T,obj.P,obj.Q,obj.S] = refpropm('TPQS','H',obj.H,'D',obj.D,obj.Subst);
%           obj.Status = 'Defined by User Input HD';
%        end
      
      function obj = DefineBy_HS(obj)
         [obj.T,obj.P,obj.D,obj.Q] = refpropm('TPDQ','H',obj.H,'S',obj.S,obj.Subst);
         obj.Status = 'Defined by User Input HS';
      end
%% S Kombis
%        function obj = DefineBy_ST(obj)
%           [obj.D,obj.P,obj.Q,obj.H] = refpropm('DPQH','S',obj.S,'T',obj.T,obj.Subst);
%           obj.Status = 'Defined by User Input ST';
%        end    
% 
%        function obj = DefineBy_SP(obj)
%           [obj.T,obj.D,obj.H,obj.Q] = refpropm('TDHQ','S',obj.S,'P',obj.P,obj.Subst);
%           obj.Status = 'Defined by User Input SP';
%        end
%       
%        function obj = DefineBy_SD(obj)
%           [obj.T,obj.P,obj.Q,obj.H] = refpropm('TPQH','S',obj.S,'D',obj.D,obj.Subst);
%           obj.Status = 'Defined by User Input SD';
%        end    
% 
%        function obj = DefineBy_SH(obj)
%           [obj.T,obj.P,obj.D,obj.Q] = refpropm('TPDQ','S',obj.S,'H',obj.H,obj.Subst);
%           obj.Status = 'Defined by User Input SH';
%        end
%       
%        function obj = DefineBy_SQ(obj)
%           [obj.T,obj.P,obj.H,obj.D] = refpropm('TPHD','S',obj.S,'Q',obj.Q,obj.Subst);
%           obj.Status = 'Defined by User Input SQ';
%        end
%%
      function TCelsius = ConvertTinCelsius(obj)
         TCelsius = obj.T - 273.15;
      end
   end
end