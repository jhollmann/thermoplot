close all
T_cr = 273.15+94.7;
T_cr2 =refpropm('T','C',0,' ',0,'water')
T_tr2 =refpropm('T','R',0,' ',0,'water')
p_cr = 33.822*100;
%s_cr = refpropm('S','T',T_cr,'P',p_cr*0.99,'R1234yf');
%h_cr = refpropm('H','T',T_cr,'P',p_cr*0.99,'R1234yf');

T_tr = 273.15 -53.15;
p_tr = 0.31508*100;


% Siedelinie
T = linspace(T_tr,T_cr,1000);

for i=1:length(T)
    p_s(i) = refpropm('P','T',T(i),'Q',0,'water');
    s_s(i) = refpropm('S','T',T(i),'Q',0,'water');
    h_s(i) = refpropm('H','T',T(i),'Q',0,'water');
end

for i=1:length(T)
    p_t(i) = refpropm('P','T',T(i),'Q',1,'water');
    s_t(i) = refpropm('S','T',T(i),'Q',1,'water');
    h_t(i) = refpropm('H','T',T(i),'Q',1,'water');
end

figure(1)
plot(s_s,(T),'k')
hold on
plot(s_t,(T),'k')
xlabel('Entropie in J/(kg K)')
ylabel('Temperatur in �C')

figure(2)
semilogy(h_s,p_s,'k')
hold on
semilogy(h_t,p_t,'k')
xlabel('Enthalpie in J/kg')
ylabel('Druck in kPa')

% Isobare f�r Ts bei 10 bar
svar = linspace(800,1800,1000);%s_s(1),s_t(end),1000);
pconst = 10*100;
for i=1:length(svar)
    try
        Tisobar(i) = refpropm('T','P',pconst,'S',svar(i),'water');
    catch
        Tisobar(i) = 0;
    end
end

figure(1)
plot(svar,Tisobar,'Color',[0.5 0.5 0.5])


% Isobare f�r Ts bei 1 bar
svar = linspace(800,1800,1000);%s_s(1),s_t(end),1000);
pconst = 1*100;
for i=1:length(svar)
    try
        Tisobar(i) = refpropm('T','P',pconst,'S',svar(i),'water');
    catch
        Tisobar(i) = 0;
    end
end

figure(1)
plot(svar,Tisobar,'Color',[0.5 0.5 0.5])


% Isotherme f�r logph bei 40�C
hvar = linspace(2e5,5e5,1000);
Tconst = 273.15+40;
for i=1:length(hvar)
    try
        pisotherm(i) = refpropm('P','T',Tconst,'H',hvar(i),'water');
    catch
        %disp(hvar)
        pisotherm(i) = 0;
    end
end

figure(2)
semilogy(hvar,pisotherm,'Color',[0.5 0.5 0.5])

% Isotherme f�r logph bei 0�C
hvar = linspace(2e5,5e5,1000);
Tconst = 273.15+0;
for i=1:length(hvar)
    try
        pisotherm(i) = refpropm('P','T',Tconst,'H',hvar(i),'water');
    catch
        %disp(hvar)
        pisotherm(i) = 0;
    end
end

figure(2)
semilogy(hvar,pisotherm,'Color',[0.5 0.5 0.5])

% Isotherme f�r logph bei 80�C
hvar = linspace(2e5,5e5,1000);
Tconst = 273.15+80;
for i=1:length(hvar)
    try
        pisotherm(i) = refpropm('P','T',Tconst,'H',hvar(i),'water');
    catch
        %disp(hvar)
        pisotherm(i) = 0;
    end
end

figure(2)
semilogy(hvar,pisotherm,'Color',[0.5 0.5 0.5])

% Verdampfung bei p=10 bar

qvar = linspace(0,1,100);

for i=1:length(qvar)
   p_verd(i) = refpropm('P','P',(10*100),'Q',qvar(i),'water');
    T_verd(i) = refpropm('T','P',(10*100),'Q',qvar(i),'water');
   h_verd(i) = refpropm('H','P',(10*100),'Q',qvar(i),'water');
   s_verd(i) = refpropm('S','P',(10*100),'Q',qvar(i),'water');
end
figure(1)
plot(s_verd,T_verd,'b')

figure(2)
semilogy(h_verd,p_verd,'b')

% Entspannung isentrop

sconst = s_verd(end);

pvar = linspace(p_verd(end),100,100);

for i=1:length(pvar)
   T_ent(i) = refpropm('T','P',pvar(i),'S',sconst,'water');
   s_ent(i) = refpropm('S','P',pvar(i),'S',sconst,'water');
   h_ent(i) = refpropm('H','P',pvar(i),'S',sconst,'water');
   p_ent(i) = refpropm('P','P',pvar(i),'S',sconst,'water');
end

figure(1)
plot(s_ent,T_ent,'b')

figure(2)
semilogy(h_ent,p_ent,'b')

% Entspannung �ber isentropen Wirkungsgrad

eta_sT = 0.75;

s1 = s_verd(end);
h1 = h_verd(end);
for i=1:length(pvar)
   h2s(i) = refpropm('H','P',pvar(i),'S',s1,'water');
   
   h2(i) = h1 - eta_sT * (h1 - h2s(i));
   
   T2(i) = refpropm('T','P',pvar(i),'H',h2(i),'water');
   s2(i) = refpropm('S','P',pvar(i),'H',h2(i),'water');
   p2(i) = refpropm('P','P',pvar(i),'H',h2(i),'water');
end

figure(1)
plot(s2,T2,'b--')

figure(2)
semilogy(h2,p2,'b--')

disp('Test2')

