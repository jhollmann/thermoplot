clear all

% Definiere Zustandspunkt durch Vorgabe von Temperatur und Druck
SP01 = StatePoint;

SP01.Subst = 'Water';
SP01.T = 300; %K
SP01.P = 300; %kPa (entspricht 2 bar)

SP01 = DefineBy_TP(SP01);

%%

% Definiere Zustandspunkt durch Vorgabe von Druck und Dampfanteil
SP02 = StatePoint;

SP02.Subst = 'water';
SP02.P = 300; %kPa
SP02.Q = 0; %Zustand auf Siedelinie

SP02 = DefineBy_PQ(SP02);

%%

SP03 = StatePoint;

SP03.Subst = 'water';
SP03.P = 300; %kPa
SP03.Q = 1;

SP03 = DefineBy_PQ(SP03);

%%

SP04 = StatePoint;

SP04.Subst = 'water';
SP04.T = 300;
SP04.Q = 1;

SP04 = DefineBy_TQ(SP04);

%%

delta_h_evap = SP03.H - SP02.H;

disp(SP03.T)
disp(ConvertTinCelsius(SP03))
